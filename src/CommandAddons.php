<?php
declare(strict_types=1);

namespace Zworo\LaravelConsoleAddons;

use Illuminate\Console\Command;
use Illuminate\Console\OutputStyle;
use InvalidArgumentException;

class CommandAddons extends Command
{
    const SPACING = '    ';
    const SEPARATOR = '-';

    const COLOR_BLACK = 'black';
    const COLOR_RED = 'red';
    const COLOR_GREEN = 'green';
    const COLOR_YELLOW = 'yellow';
    const COLOR_BLUE = 'blue';
    const COLOR_MAGENTA = 'magenta';
    const COLOR_CYAN = 'cyan';
    const COLOR_WHITE = 'white';

    /**
     * Print text in selected color
     * @param string $text
     * @param string $color
     * @param int $spacing
     * @param bool $newLine
     */
    public function text(string $text, string $color = self::COLOR_WHITE, int $spacing = 0, bool $newLine = false)
    {
        $margin = str_repeat(self::SPACING, $spacing);
        $lines = explode("\n", $text);
        $count = count($lines);
        for ($i = 0; $i < $count - 1; $i++) {
            $this->output->write($this->colorString($margin . $lines[$i], $color), true);
        }
        $this->output->write($this->colorString($margin . $lines[$count - 1], $color), $newLine);
    }

    /**
     * Print text in selected color ending with new line
     * @param string $text
     * @param string $color
     * @param int $spacing
     */
    public function textLn(string $text, string $color = 'white', int $spacing = 0)
    {
        $this->text($text, $color, $spacing, true);
    }

    public function task(string $text, \Closure $func)
    {
        $result = null;
        $this->text($text);
        try {
            $result = call_user_func($func);
        } catch (\Exception $e) {
            $this->textLn(' ✖', static::COLOR_RED);
            throw $e;
        }
        $this->textLn(' ✔', static::COLOR_GREEN);
        return $result;
    }

    /**
     * Print new line
     * @param int $count
     */
    public function newLine($count = 1)
    {
        $this->output->newLine($count);
    }

    /**
     * Pretty print array
     * @param array $array
     * @param string $color
     * @param int $spacing
     */
    public function printArray(array $array, string $color = 'white', int $spacing = 0)
    {
        $this->textLn(json_encode($array, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES), $color, $spacing);
    }

    /**
     * Print progress
     * @param $current
     * @param $max
     * @param string|null $text
     * @param bool $showDate
     * @param string $color
     * @param int $spacing
     */
    public function printProgress(
            $current,
            $max,
            string $text = null,
            bool $showDate = true,
            string $color = 'white',
            int $spacing = 0,
            bool $newLine = true
    ) {
        $perc = $max == 0 ? 100 : round($current / $max * 100);
        $this->text('[ ', static::COLOR_WHITE, $spacing);
        $this->text(sprintf('%3d%%', $perc), static::COLOR_CYAN);
        if ($showDate) {
            $date = date('Y-m-d H:i:s');
            $this->text(' - ' . $date, static::COLOR_CYAN);
        }
        $this->text(' ] ', static::COLOR_WHITE);
        if ($text) {
            $this->text($text, $color);
        }
        if ($newLine) {
            $this->newLine();
        }
    }

    /**
     * Print separator
     * @param int $length
     * @param int $newLinesBefore
     * @param int $newLinesAfter
     */
    public function separator(int $length = 64, int $newLinesBefore = 1, int $newLinesAfter = 1)
    {
        $this->newLine($newLinesBefore);
        $separatorString = str_repeat(self::SEPARATOR, $length);
        $this->textLn($separatorString);
        $this->newLine($newLinesAfter);
    }

    /**
     * Prepare colored message
     * @param string $text
     * @param string $color
     * @return string
     */
    protected function colorString(string $text, string $color): string
    {
        if (!in_array($color, $this->getColorsArray())) {
            throw new InvalidArgumentException('Invalid foreground color specified: "' . $color . '". ' .
                    'Expected one of (' . implode(', ', $this->getColorsArray()) . ')');
        }
        $style = "fg=$color";
        return "<$style>" . $text . "</$style>";
    }

    /**
     * @return array
     */
    private function getColorsArray(): array
    {
        return [
                self::COLOR_BLACK,
                self::COLOR_RED,
                self::COLOR_GREEN,
                self::COLOR_YELLOW,
                self::COLOR_BLUE,
                self::COLOR_MAGENTA,
                self::COLOR_CYAN,
                self::COLOR_WHITE
        ];
    }

    /**
     * Info: only for testing purposes
     * @param OutputStyle $output
     */
    public function setOutput(OutputStyle $output)
    {
        $this->output = $output;
    }
}
