<?php
declare(strict_types=1);

namespace Zworo\LaravelConsoleAddons\Test;

use Illuminate\Console\OutputStyle;
use Symfony\Component\Console\Input\ArgvInput;
use Zworo\LaravelConsoleAddons\CommandAddons;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\Output;


class CommandAddonsTest extends TestCase
{
    protected $commonAddons;
    protected $output;

    public function setUp(): void
    {
        parent::setUp();

        $this->output = new TestOutput();
        $class = new CommandAddons();
        $class->setOutput(new OutputStyle(new ArgvInput(), $this->output));
        $this->commonAddons = $class;
    }

    /**
     * Return method from given class accessible
     * @param string $classname
     * @param string $methodName
     * @return \ReflectionMethod
     * @throws \ReflectionException
     */
    protected function getMethodPublic(string $classname, string $methodName)
    {
        $class = new \ReflectionClass($classname);
        $method = $class->getMethod($methodName);
        $method->setAccessible(true);
        return $method;
    }

    /**
     * @test
     * @group command
     */
    public function textTest()
    {
        $method = $this->getMethodPublic(CommandAddons::class, 'text');

        $method->invoke($this->commonAddons, 'abc');
        $this->assertEquals('abc', $this->output->output);

        $this->output->clear();
        $text = <<<EOF
a
 b
   c
EOF;
        $expected = <<<EOF
        a
         b
           c
EOF;

        $method->invoke($this->commonAddons, $text, CommandAddons::COLOR_WHITE, 2);
        $this->assertEquals($expected, $this->output->output);

        $this->output->clear();
        $method->invoke($this->commonAddons, 'ghi', CommandAddons::COLOR_WHITE, 0, true);
        $this->assertEquals("ghi\n", $this->output->output);
    }

    /**
     * @test
     * @group command
     */
    public function textLnTest()
    {
        $method = $this->getMethodPublic(CommandAddons::class, 'textLn');
        $method->invoke($this->commonAddons, 'abc');
        $this->assertEquals("abc\n", $this->output->output);
    }

    /**
     * @test
     * @group command
     */
    public function printArrayTest()
    {
        $arr = [
                'one' => 'abc',
                'two' => [
                        [3, 'b', 'f']
                ]
        ];
        $expected = <<<EOF
{
    "one": "abc",
    "two": [
        [
            3,
            "b",
            "f"
        ]
    ]
}

EOF;
        $method = $this->getMethodPublic(CommandAddons::class, 'printArray');
        $method->invoke($this->commonAddons, $arr);
        $this->assertEquals($expected, $this->output->output);
    }

    /**
     * @test
     * @group command
     */
    public function separatorTest()
    {
        $method = $this->getMethodPublic(CommandAddons::class, 'separator');
        $method->invoke($this->commonAddons, 4, 2, 3);
        $this->assertEquals("\n\n----\n\n\n\n", $this->output->output);
    }

    /**
     * @test
     * @group command
     */
    public function printProgressTest()
    {
        $method = $this->getMethodPublic(CommandAddons::class, 'printProgress');

        $method->invoke($this->commonAddons, 1, 2, 'tekst', false);
        $this->assertEquals("[  50% ] tekst\n", $this->output->output);

        $this->output->clear();
        $method->invoke($this->commonAddons, 1, 7, null, false);
        $this->assertEquals("[  14% ] \n", $this->output->output);

        $this->output->clear();
        $method->invoke($this->commonAddons, 1, 3, 'tekst', false, CommandAddons::COLOR_WHITE, 3);
        $this->assertEquals("            [  33% ] tekst\n", $this->output->output);

        $this->output->clear();
        $method->invoke($this->commonAddons, 1, 2, 'tekst', true);
        $this->assertEquals("[  50% - " . date('Y-m-d H:i:s') . " ] tekst\n", $this->output->output);

        $this->output->clear();
        $method->invoke($this->commonAddons, 1, 2, 'tekst', true, CommandAddons::COLOR_WHITE, 0, false);
        $this->assertEquals("[  50% - " . date('Y-m-d H:i:s') . " ] tekst", $this->output->output);
    }

    /**
     * @test
     * @group command
     */
    public function colorStringTest()
    {
        $method = $this->getMethodPublic(CommandAddons::class, 'colorString');

        $result = $method->invoke($this->commonAddons, 'abc', CommandAddons::COLOR_RED);
        $this->assertEquals("<fg=red>abc</fg=red>", $result);

        $this->expectException(\InvalidArgumentException::class);
        $method->invoke($this->commonAddons, 'abc', 'iulskdhfas');
    }

    /**
     * @test
     * @group command
     */
    public function setOutputTest()
    {
        $class = new CommandAddons();
        $this->assertEquals(null, $class->getOutput());

        $outputStyle = new OutputStyle(new ArgvInput(), $this->output);
        $class->setOutput($outputStyle);
        $this->assertEquals($outputStyle, $class->getOutput());
    }
}

class TestOutput extends Output
{
    public $output = '';

    public function clear()
    {
        $this->output = '';
    }

    protected function doWrite($message, $newline)
    {
        $this->output .= $message . ($newline ? "\n" : '');
    }

    public function newLine(int $count = 1)
    {
        $sep = str_repeat("\n", $count);
        $this->output .= $sep;
    }
}
